import React, { useState } from 'react'
import {
  View,
  StyleSheet,
  TextInput,
  Button,
  ScrollView,
  TouchableWithoutFeedback,
  Keyboard
} from 'react-native'
import { useDispatch } from 'react-redux'
import { addPost } from '../store/actions/post'

export const CreateScreen = ({ navigation }) => {
  const dispatch = useDispatch()
  const [text, setText] = useState('')

  const saveHandler = () => {
    const post = {
      date: new Date().toJSON(),
      text: text,
      booked: false
    }
    dispatch(addPost(post))
    navigation.navigate('Posts')
  }

  return (
    <ScrollView>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.wrapper}>
          <TextInput
            style={styles.textarea}
            placeholder='Enter text'
            value={text}
            onChangeText={setText}
            multiline
          />
          <Button
            title='Save'
            onPress={saveHandler}
            disabled={!text}
          />
        </View>
      </TouchableWithoutFeedback>
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  wrapper: {
    padding: 10
  },
  title: {
    fontSize: 20,
    textAlign: 'center',
     marginVertical: 10
  },
  textarea: {
    padding: 10,
    marginBottom: 10
  }
})
