
import React, { useEffect, useCallback } from 'react'
import {
  View,
  Text,
  StyleSheet,
  Button,
  ScrollView,
  Alert
} from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { toogleBooked, removePost } from '../store/actions/post'

export const PostScreen = ({ navigation, route }) => {
  const dispatch = useDispatch()
  const postId = route.params?.postId

  const post = useSelector(state =>
    state.post.allPosts.find(p => p.id === postId)
  )

  const booked = useSelector(state =>
    state.post.bookedPosts.some(post => post.id === postId)
  )

  const openPostHandler = post => {
    navigation.navigate('Post', {
      postId: post.id,
      date: post.date,
      booked: post.booked
    })
  }

  useEffect(() => {
    navigation.setParams({ booked })
  }, [booked])

  const toggleHandler = useCallback(() => {
    dispatch(toogleBooked(post))
  }, [dispatch, post])

  useEffect(() => {
    navigation.setOptions({ toggleHandler })
  }, [toggleHandler])

  const removeHandler = () => {
    Alert.alert(
      'Delete',
      'post?',
      [
        {
          text: 'Cancel',
          style: 'cancel'
        },
        {
          text: 'Delete',
          style: 'destructive',
          onPress() {
            navigation.navigate('Posts')
            dispatch(removePost(postId))
          }
        }
      ],
      { cancelable: false }
    )
  }

  if (!post) {
    return null
  }

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={toggleHandler} title="Toggle booked" />
      ),
    })
  }, [navigation])

  return (
    <ScrollView>
      <View style={styles.textWrap}>
        <Text>
          {post.text}
          {post.booked ? ' *' : ''}
        </Text>
      </View>
      <Button
        title='Delete'
        onPress={removeHandler}
      />
    </ScrollView>
  )
}

const styles = StyleSheet.create({
  textWrap: {
    padding: 10
  },
})
