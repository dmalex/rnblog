import React, { useEffect } from 'react'
import { View, StyleSheet, Button, ActivityIndicator } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import { PostList } from '../components/PostList'
import { loadPosts } from '../store/actions/post'
import { DB } from '../db'

export const MainScreen = ({ navigation }) => {
  const dispatch = useDispatch()

  const openPostHandler = post => {
    navigation.navigate('Post', {
      postId: post.id,
      date: post.date,
      booked: post.booked
    })
  }

  useEffect(() => {
    DB.init()
    dispatch(loadPosts())
  }, [dispatch])

  const allPosts = useSelector(state => state.post.allPosts)
  const loading = useSelector(state => state.post.loading)

  React.useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button onPress={() => navigation.navigate('Create')} title="Create" />
      ),
      headerLeft: () => (
        <Button onPress={() => navigation.navigate('Booked')} title="Booked" />
      ),
    })
  }, [navigation])

  if (loading) {
    return (
      <View style={styles.center}>
        <ActivityIndicator />
      </View>
    )
  }
  return <PostList data={allPosts} onOpen={openPostHandler} />
}

const styles = StyleSheet.create({
  center: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
