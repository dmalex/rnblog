import { NavigationContainer } from "@react-navigation/native"
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import "react-native-gesture-handler"
import { Provider } from 'react-redux'
import store from './store'
import { MainScreen } from "./screens/MainScreen"
import { CreateScreen } from "./screens/CreateScreen"
import { PostScreen } from "./screens/PostScreen"
import { BookedScreen } from "./screens/BookedScreen"

const Stack = createNativeStackNavigator()

function AppNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Posts">
        <Stack.Screen name="Posts">
          {props => <MainScreen {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Booked">
          {props => <BookedScreen {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Post">
          {props => <PostScreen {...props} />}
        </Stack.Screen>
        <Stack.Screen name="Create">
          {props => <CreateScreen {...props} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default function App() {
    return (
      <Provider store={store}>
        <AppNavigation />
      </Provider>
    )
}
